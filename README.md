# This is connected component labeling and bounding box calculation using java
Code here is a good starting point
https://courses.cs.washington.edu/courses/cse576/02au/homework/hw3/ConnectComponent.java

Good algorithm explanation here:
http://www.codeproject.com/Articles/336915/Connected-Component-Labeling-Algorithm
and here:
http://codeding.com/articles/connected-sets-labeling


* I assume input image is binary (1 for fg, 0 for bg)
* I need to add a way of retrieving bounding boxes ((x1,y1),(x2,y2)) of components



